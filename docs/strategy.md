<div align="center">
  <h1>Strategy</h1>
</div>

<div align="center">
  <img src="strategy_icon.png" alt="drawing" width="250"/>
</div>

## Table of Contents

1. **[What is it?](#what-is-it)**
    1. [Real-World Analogy](#real-world-analogy)
    2. [Participants](#participants)
    3. [Collaborations](#collaborations)
2. **[When do you use it?](#when-do-you-use-it)**
    1. [Motivation](#motivation)
    2. [Known Uses](#known-uses)
    3. [Categorization](#categorization)
    4. [Aspects that can vary](#aspects-that-can-vary)
    5. [Solution to causes of redesign](#solution-to-causes-of-redesign)
    6. [Consequences](#consequences)
    7. [Relations with Other Patterns](#relations-with-other-patterns)
3. **[How do you apply it?](#how-do-you-apply-it)**
    1. [Structure](#structure)
    2. [Variations](#variations)
    3. [Implementation](#implementation)
4. **[Sources](#sources)**

<br>
<br>

## What is it?

> :large_blue_diamond: **Strategy is a behavioral pattern that lets you define a family of algorithms and make their use
interchangeable.**

### Real-World Analogy

_Modes of transportation._

Going by bike, car, bus, or train are different strategies to get to a destination.

### Participants

- :bust_in_silhouette: **Strategy**
    - Defines an interface to:
        - `execute` the algorithm.
- :man: **ConcreteStrategy**
    - Provides an implementation for:
        - `execute`: use a specific algorithm
- :man: **Context**
    - Is configured with a ConcreteStrategy object.
    - Optionally implements:
        - `setStrategy`: set ConcreteStrategy dynamically.

### Collaborations

A **ConcreteStrategy** is provided to the **Context**, either in the constructor or through a method.

The **Context** forwards requests from clients to its **Strategy**.


<br>
<br>

## When do you use it?

> :large_blue_diamond: **When there are many (future) variants of behaviour that each apply in different circumstances,
or dynamically.<br>**

### Motivation

- How do we make it easy to add and modify algorithms used by our application in the future?
- How can we control which algorithm is used in different circumstances or even dynamically?
- How can we isolate algorithm specific data and implementation from the context in which it's used?

### Known Uses

- Controlling Output:
    - Text formatting: (HTML, mark down, plain text) for different media (webpage, email, print).
    - Compression: (ZIP, RAR, GZIP) depending on required speed and file size.
    - Document generation: (PDF, HTML, XML) based on preferences and system requirements.
- Algorithmic Selection:
    - Trading strategy: (momentum, mean reversion, arbitrage) for different market conditions.
    - Sorting: (Bubble sort, quicksort, merge sort) for different sizes or types of data to be sorted.
    - Payment processing: (Credit card, PayPal, Bitcoin) based on user preferences.
    - Shipping cost calculations: based on different factors (country, type of package, shipping speed).
- Behavioral Adaptation:
    - Game development: Player can dynamically select weapon (bow, sword, shield, magic staff).
    - Map routing: for possible user preference (Fastest route, shortest route, avoid highways).
    - Scheduling: based on management decisions (Round-robin, priority-based, shortest job first).
    - Validation: (Numeric field only accepts digits, postal code has to be a certain length).

### Categorization

Purpose:  **Behavioral**  
Scope:    **Object**   
Mechanisms: **Polymorphism**, **Composition**

Behavioral patterns are concerned with the assignment of responsibilities between objects.
They’re not just patterns of objects, but also the patterns of communication between those objects.

Behavioral **object** patterns use composition to achieve cooperation between objects.
An important issue here is how these peer objects know about each other.

The strategy pattern achieves cooperation by encapsulating behavior in an object and delegating requests to it

### Aspects that can vary

- The algorithm.

### Solution to causes of redesign

- Algorithmic dependencies.
    - Algorithms are often extended, optimized, and replaced, forcing dependants to also change.
- Extending functionality by subclassing.
    - Hard to understand: comprehending a subclass requires in-depth knowledge of all parent classes.
    - Hard to change: a modification to a parent might break multiple children classes.
    - Hard to reuse partially: all class members are inherited, even when not applicable, which can lead to a class
      explosion.

### Consequences

| Advantages                                                                                                                                                                      | Disadvantages                                                                                                                                                                                               |
|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| :heavy_check_mark: **Promotes reuse.** <br> Strategy classes form a family of algorithms or behaviors for reuse by different contexts.                                          | :x: **Client knowledge about strategies.** <br> A client must understand how Strategies differ before it can select the appropriate one.                                                                    |
| :heavy_check_mark: **Flexible alternative to subclassing** <br> Lets you vary the algorithm independently from it's context, making it easier to switch, understand and extend. | :x: **Communication overhead.** <br> The Strategy interface is shared by all ConcreteStrategy classes. Hence it's likely that some ConcreteStrategies won't use all the information that is passed to them. |
| :heavy_check_mark: **Eliminates conditional statements.** <br> Offers an alternative to conditional statements for selecting desired behavior.                                  | :x: **Increased number of objects.** <br> Strategies increase the number of objects in an application.                                                                                                      |
| :heavy_check_mark: **Choice of implementation** <br> Client can choose among strategies with the same behavior but different time and space trade-offs.                         |                                                                                                                                                                                                             |

### Relations with Other Patterns

_Distinction from other patterns:_

- Allowing to switch implementation at runtime is the main reason why the Bridge is confused with the Strategy pattern.
    - While the Strategy pattern is meant for behavior, the Bridge pattern is meant for structure.
- Decorator changes behavior of an object by wrapping it from the outside (changing its skin).
    - The strategy pattern allows you to modify the object's behavior by changing its internals (changing the guts).
    - Strategy is a better choice:
        - If the object has many methods (which would otherwise all have to be wrapped by the decorator).
        - When the methods must differ from the component. A strategy can have its own specialized interface, whereas a
          decorator's interface must conform to the component's.
    - Decorators are the better choice if it's hard or impossible to modify the object itself (such that it would
      accept a strategy).
- Command and Strategy may look similar because you can use both to parameterize an object with some action. However,
  they have very different intents.
    - You can use Command to convert any operation into an object. The conversion lets you defer execution of the
      operation, queue it, store the history of commands, send commands to remote services, etc.
    - On the other hand, Strategy usually describes different ways of doing the same thing, letting you swap these
      algorithms within a single context class.
- Template Method is based on inheritance: it lets you alter parts of an algorithm by extending those parts in
  subclasses.
    - Strategy is based on composition: you can alter parts of the object’s behavior by supplying it with
      different strategies that correspond to that behavior.
    - Template Method works at the class level, so it’s static.
    - Strategy works on the object level, letting you switch behaviors at runtime.
- Both State and Strategy change the behavior of the context by using composition.
    - Strategy makes these objects completely independent and unaware of each other.
    - However, State doesn’t restrict dependencies between concrete states objects.

_Combination with other patterns:_

- Strategy objects often make good flyweights.

<br>
<br>

## How do you apply it?

> :large_blue_diamond: **A Context class that stores a Strategy object, and uses it to execute an algorithm.**

### Structure

```mermaid
classDiagram
    class Context {
        -strategy: Strategy
        +Context(strategy: Strategy)
        +setStrategy(strategy: Strategy)
        +executeStrategy()
    }
    class Strategy {
        <<interface>>
        +execute()*
    }
    class ConcreteStrategyA {
        +execute()
    }
    class ConcreteStrategyB {
        +execute()
    }

    Context *-- Strategy: composes
    Strategy <|.. ConcreteStrategyA: implements
    Strategy <|.. ConcreteStrategyB: implements
```

### Variations

_Access to data from Context:_

- **Parameters**: Data from the Context is passed to the Strategy as parameters.
    - :heavy_check_mark: Strategy decoupled from Context.
    - :x: Context might pass data that the Strategy doesn't need.
- **Delegate**: Context passes itself as argument to Strategy or Strategy stores a reference to the Context.
    - :heavy_check_mark: Strategy can request exactly what it needs.
    - :x: Context must define a more elaborate interface to its data.
    - :x: Strategy more tightly coupled to Context.

_Class or Function Strategy:_

- **Class**: Strategies are implemented as classes.
    - :heavy_check_mark: Strategy can be instantiated with algorithm specific data.
        - Use dataclasses and set values when instantiating the class.
        - [ArjanCodes: Solving A Common Issue With The Strategy Pattern // In Python](https://youtu.be/UPBOOIRrl40?si=r_kFPIveUePNPAVH)
    - :x: More boilerplate code.
- **Function**: Strategies are implemented as functions.
    - :heavy_check_mark: Simple and less code.
    - :x: Harder to add algorithm specific data.
    - :x: No private methods to break up a long or complex algorithm.

### Implementation

In the example we apply the strategy pattern to a rating system.
It calculates the best scoring streaming service based on the selected metric.

The strategy pattern is implemented using functions.
Because of that, the typehint of the context defines the expected interface: `Callable[[Streamer], int]`

- [Strategy (interface)](../strategy/pattern/stream_scorer.py)
- [Concrete Strategy](../strategy/pattern/scoring_strategies.py)
- [Context](../strategy/pattern/stream_scorer.py)

The _client_ is a streamer service rating program:

- [Client](../strategy/clients/stream_compare.py)

The unit tests exercise the public interface of the Strategy and Context :

- [Strategy tests](../tests/unit/test_scoring_strategies.py)
- [Context tests](../tests/unit/test_stream_scorer.py)

<br>
<br>

## Sources

- [Design Patterns: Elements of Reusable Object-Oriented Software](https://www.amazon.com/Design-Patterns-Elements-Reusable-Object-Oriented/dp/0201633612)
- [Refactoring Guru - Strategy](https://refactoring.guru/design-patterns/strategy)
- [Head First Design Patterns: A Brain-Friendly Guide](https://www.amazon.com/Head-First-Design-Patterns-Brain-Friendly/dp/0596007124)
- [ArjanCodes: The Strategy Pattern: Write BETTER PYTHON CODE Part 3](https://youtu.be/WQ8bNdxREHU?si=OgfttvA2y2Z1833t)
- [ArjanCodes: Solving A Common Issue With The Strategy Pattern // In Python](https://youtu.be/UPBOOIRrl40?si=r_kFPIveUePNPAVH)
- [ArjanCodes: Variations of the Strategy Pattern // Using Python Features!](https://youtu.be/n2b_Cxh20Fw?si=HK7FiENL2AFA1mG5)
- [Gui Ferreira: The Design Pattern Everyone MUST Know!](https://youtu.be/_kV7lPOLISw?si=3xAFNPEJWtKmscg5)
- [Geekific: The Strategy Pattern Explained and Implemented in Java](https://youtu.be/Nrwj3gZiuJU?si=XIXxA_0bWYOrOHvi)
