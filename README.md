<h1 style="text-align: center;">Strategy App</h1>

## Theory

For a description of the Strategy design pattern, you can refer
to the [Strategy](docs/strategy.md) summary.

## Usage

### Prerequisites

Executing these commands requires an installed version
of [Docker Compose](https://docs.docker.com/compose/install/).

### Running the application

    docker compose run --rm strategy_app

<div align="center">
<img src="docs/strategy_demo.gif" alt="drawing"/>
</div>

### Running the tests

    docker compose run --rm strategy_app poetry run tests

## Development setup

The included project setting files can be used to continue development in PyCharm.

For a detailed description refer to [PyCharm project setup](docs/pycharm_project_setup.md).


