from strategy.clients.stream_compare import compare_streamers
from strategy.lib.score import Score, Streamer
from tests.test_doubles.spies import ScoreSpy


class StreamCompareDriver:
    def __init__(self) -> None:
        self._score_spy = ScoreSpy()
        disney = Streamer(
            name="Disney +",
            current_user_count=500,
            previous_user_count=400,
            users_canceled_count=25,
            home_page_visits=200,
            subscribe_page_visits=150,
        )
        netflix = Streamer(
            name="Netflix",
            current_user_count=800,
            previous_user_count=600,
            users_canceled_count=20,
            home_page_visits=100,
            subscribe_page_visits=10,
        )

        amazon = Streamer(
            name="Amazon Prime",
            current_user_count=420,
            previous_user_count=400,
            users_canceled_count=50,
            home_page_visits=80,
            subscribe_page_visits=5,
        )

        self._streamers = [disney, netflix, amazon]
        self._strategy_name = ""

    def score_by(self, strategy_name: str) -> None:
        self._strategy_name = strategy_name

    def assert_highest_score(self, name: str, points: int) -> None:
        compare_streamers(
            strategy_name_selector=lambda: self._strategy_name,
            score_presenter=self._score_spy.set_score,
            streamers=self._streamers,
        )
        self.assert_equals(Score(name, points), self._score_spy.get_score())

    @staticmethod
    def assert_equals(expected: Score, actual: Score) -> None:
        assert expected == actual, f"expected: '{expected}', but got '{actual}'"
