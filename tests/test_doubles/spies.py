from strategy.lib.score import Score


class ScoreSpy:
    def __init__(self) -> None:
        self._score = Score("", 0)

    def set_score(self, score: Score) -> None:
        self._score = score

    def get_score(self) -> Score:
        return self._score
