import unittest

from tests.sut_drivers.stream_compare_driver import StreamCompareDriver


class TestStreamScorer(unittest.TestCase):
    def test_conversion_rate_score(self) -> None:
        comparer = StreamCompareDriver()

        comparer.score_by("conversion rate")

        comparer.assert_highest_score("Disney +", points=75)

    def test_growth_rate_score(self) -> None:
        comparer = StreamCompareDriver()

        comparer.score_by("growth rate")

        comparer.assert_highest_score("Netflix", points=33)

    def test_churn_score(self) -> None:
        comparer = StreamCompareDriver()

        comparer.score_by("churn")

        comparer.assert_highest_score("Amazon Prime", points=12)
