import unittest

from strategy.lib.score import Streamer
from strategy.pattern.scoring_strategies import (
    score_churn_strategy,
    score_conversion_rate_strategy,
    score_growth_rate_strategy,
)


class TestRatingStrategies(unittest.TestCase):
    def setUp(self) -> None:
        self._streamer = Streamer(
            name="Disney",
            current_user_count=500,
            previous_user_count=400,
            users_canceled_count=25,
            home_page_visits=200,
            subscribe_page_visits=150,
        )

    def test_score_conversion_rate_strategy(self) -> None:
        conversion_rate_score = score_conversion_rate_strategy(self._streamer)
        self.assertEqual(75, conversion_rate_score)

    def test_score_growth_rate_strategy(self) -> None:
        growth_rate_score = score_growth_rate_strategy(self._streamer)
        self.assertEqual(25, growth_rate_score)

    def test_score_churn_strategy(self) -> None:
        churn_rate_score = score_churn_strategy(self._streamer)
        self.assertEqual(6, churn_rate_score)
