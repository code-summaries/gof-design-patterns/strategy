import unittest

from strategy.lib.score import Score, Streamer
from strategy.pattern.stream_scorer import StreamScorer


class TestRater(unittest.TestCase):
    def setUp(self) -> None:
        self.streamer_1 = Streamer("fake1", 0, 0, 0, 0, subscribe_page_visits=1)
        self.streamer_2 = Streamer("fake2", 0, 0, 0, 0, subscribe_page_visits=2)
        self.rater = StreamScorer(score_strategy=lambda s: s.subscribe_page_visits)

    def test_rater_returns_highest_rated_out_of_one(self) -> None:
        expected_rating = Score(self.streamer_1.name, points=1)
        actual_rating = self.rater.get_highest_score([self.streamer_1])
        self.assertEqual(expected_rating, actual_rating)

    def test_rater_returns_highest_rated_out_of_two(self) -> None:
        expected_rating = Score(self.streamer_2.name, points=2)
        actual_rating = self.rater.get_highest_score([self.streamer_1, self.streamer_2])
        self.assertEqual(expected_rating, actual_rating)
