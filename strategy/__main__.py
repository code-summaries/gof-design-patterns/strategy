from strategy.clients.stream_compare import compare_streamers
from strategy.lib.prompts import display_score, select_strategy_prompt
from strategy.lib.score import Streamer

if __name__ == "__main__":
    disney = Streamer(
        name="Disney +",
        current_user_count=500,
        previous_user_count=400,
        users_canceled_count=25,
        home_page_visits=200,
        subscribe_page_visits=150,
    )
    netflix = Streamer(
        name="Netflix",
        current_user_count=800,
        previous_user_count=600,
        users_canceled_count=20,
        home_page_visits=100,
        subscribe_page_visits=10,
    )
    amazon = Streamer(
        name="Amazon Prime",
        current_user_count=420,
        previous_user_count=400,
        users_canceled_count=50,
        home_page_visits=80,
        subscribe_page_visits=5,
    )

    compare_streamers(
        strategy_name_selector=select_strategy_prompt,
        score_presenter=display_score,
        streamers=[disney, netflix, amazon],
    )
