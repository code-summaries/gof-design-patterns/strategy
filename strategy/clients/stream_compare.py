from typing import Callable

from strategy.lib.prompts import get_strategy
from strategy.lib.score import Score, Streamer
from strategy.pattern.stream_scorer import StreamScorer


def compare_streamers(
    strategy_name_selector: Callable[[], str],
    score_presenter: Callable[[Score], None],
    streamers: list[Streamer],
) -> None:
    strategy_name = strategy_name_selector()
    strategy = get_strategy(strategy_name)

    stream_scorer = StreamScorer(strategy)

    score = stream_scorer.get_highest_score(streamers)
    score_presenter(score)
