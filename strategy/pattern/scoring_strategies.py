from strategy.lib.score import Streamer


def score_conversion_rate_strategy(streamer: Streamer) -> int:
    conversion_rate = streamer.subscribe_page_visits / streamer.home_page_visits
    return int(conversion_rate * 100)


def score_growth_rate_strategy(streamer: Streamer) -> int:
    user_count_growth = streamer.current_user_count - streamer.previous_user_count
    growth_rate = user_count_growth / streamer.previous_user_count
    return int(growth_rate * 100)


def score_churn_strategy(streamer: Streamer) -> int:
    churn = streamer.users_canceled_count / streamer.previous_user_count
    return int(churn * 100)
