from typing import Callable

from strategy.lib.score import Score, Streamer


class StreamScorer:
    def __init__(self, score_strategy: Callable[[Streamer], int]):
        self._score_strategy = score_strategy

    def get_highest_score(self, streamers: list[Streamer]) -> Score:
        highest_score_streamer = max(streamers, key=self._score_strategy)
        score = self._score_strategy(highest_score_streamer)
        return Score(highest_score_streamer.name, score)
