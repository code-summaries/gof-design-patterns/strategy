from typing import Callable

from strategy.lib.score import Score, Streamer
from strategy.pattern.scoring_strategies import (
    score_churn_strategy,
    score_conversion_rate_strategy,
    score_growth_rate_strategy,
)

SCORE_STRATEGIES = {
    "conversion rate": score_conversion_rate_strategy,
    "growth rate": score_growth_rate_strategy,
    "churn": score_churn_strategy,
}


def get_strategy(strategy_name: str) -> Callable[[Streamer], int]:
    strategy = SCORE_STRATEGIES.get(strategy_name)
    if not strategy:
        print("Invalid metric selected, defaulting to conversion rate.")
        strategy = score_conversion_rate_strategy
    return strategy


def select_strategy_prompt() -> str:
    options = "\n    ".join(SCORE_STRATEGIES.keys())
    prompt_text = (
        "\nWelcome to Stream Compare, the only way to compare streaming services! \n"
        "Please write the metric by which to score the streamers, and press enter: \n"
        f"    {options}\n"
    )
    return input(prompt_text)


def display_score(score: Score) -> None:
    print(f"The highest scoring streamer is: '{score.name}' with a score of: '{score.points}'")
