from dataclasses import dataclass


@dataclass
class Streamer:
    name: str
    current_user_count: int
    previous_user_count: int
    users_canceled_count: int
    home_page_visits: int
    subscribe_page_visits: int


@dataclass
class Score:
    name: str
    points: int
